# about

This script is an attempt to automate git merges using a pipeline concept.
The idea is borrowed from Bazaar pipeline but extended to complex trees.

# script usage

```
bash flow.sh flow-sample.txt
```

Comments in flow-sample.txt will explain everything ;)

