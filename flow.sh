#!/bin/bash

red="[31m"
green="[32m"
yellow="[33m"
blue="[34m"
magenta="[35m"
cyan="[36m"
norm="[37m"

set -f

function run_git() {
  echo "git ${magenta}$*${norm}" >&2
  git $@
  return $?
}

# args:
# "a b c" 'b"
function in_list() {
  for item in $1 ; do
    if [[ "$item" == "$2" ]] ; then
      return 1
    fi
  done

  return 0
}

# 0 - ok
# 1 - deltas
# 3 - bad command
function exec_oper() {
  if [[ "$1" == "merge" ]] ; then
    shift
    run_git "merge --no-ff $@"
    return $?
  fi

  if [[ "$1" == "switch" ]] ; then
    run_git "switch $2"
    return $?
  fi

  echo "${red}unexpected command $@${norm}"
  return 3
}

function exec_cmd () {
  case $1 in
    "switch")
      shift
      exec_oper switch $1
      case $? in
        0)
          echo "${green}switch $1 OK${norm}"
          return 0
          ;;
        *)
          echo "${red}switch $1 error $?${norm}"
          exit 3
          ;;
      esac

      ;;
    "merge")
      shift
      items=$@

      delta=0

      no_merged=$(run_git branch --no-merged)

      for item in $items ; do
        in_list "$no_merged" $item
        case $? in
          0)
            ;;
          1)
            delta=1
            break
            ;;
          *)
            echo "${red}other error at delta $item - return${norm}"
            exit 3
            ;;
        esac
      done

      if [[ "$delta" == "0" ]] ; then
        echo "${green}diff no delta(s) - return${norm}"
        return 0
      else
        echo "delta - running ${cyan}merge${norm}"
        exec_oper merge $@
        case $? in
          0)
            echo "merge $@ ${green}OK${norm}"
            ;;
          1)
            echo "merge $@ ${yellow}conflicts${norm}"
            return 1
            ;;
          *)
            echo "merge $@ ${red}other error${norm}"
            exit 3
            ;;
        esac
      fi
      ;;
    *)
      echo "-> bad command: $*"
      return 3
      ;;
  esac
  return 0
}

filename=$1

if [[ "$filename" == "" ]] ; then
  echo "$0: need script name as arg"
  exit 3
fi

cat $filename | while read -r line ; do
  if [[ ${line:0:1} != "#" ]] ; then
    commands=()
    for cmd in $line ; do
      commands+=($cmd)
    done
    if [[ ${#commands[@]} > 1 ]] ; then
      echo "exec command '${cyan}$line${norm}'"
      exec_cmd ${commands[@]%%}
      case $? in
        0)
          echo "command '$line' ${green}OK${norm}"
          ;;
        1)
          echo "command '$line' ${yellow}conflicts${norm} - stopping"
          exit
          ;;
        *)
          echo "command '$line' ${red}errors${norm} - stopping"
          exit
          ;;
      esac
      echo ""
    fi
  fi
done

